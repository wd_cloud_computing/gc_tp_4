// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

const express = require('express');
const cors = require('cors');

const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));

// Add middleware to authenticate requests
//app.use(myMiddleware);

app.get('/pendientes', (req, res) => {
  return admin.database().ref('/envios').orderByChild('pendiente').startAt(1).once('value', (snapshot) => {
      return res.send(snapshot.val());
  });
});

app.get('/:id', (req, res) => {
  return admin.database().ref('/envios').child(req.params.id).once('value', (snapshot) => {
      return res.send(snapshot.val());
  });
});

app.get('/', (req, res) => {
  return admin.database().ref('/envios').on('value', (snapshot) => {
      return res.send(snapshot.val());
  });
});

app.post('/', (req, res) => {
    let envio_item = req.body;
    envio_item.fechaAlta = new Date().toISOString();
    envio_item.pendiente = envio_item.fechaAlta;
    return admin.database().ref('/envios').push(envio_item).then((snapshot) => {
      return res.send(envio_item);
    });
});

app.post('/:id/movimiento', (req, res) => {
    let movimiento = req.body;
    return admin.database().ref('/envios').child(req.params.id).once('value', (snapshot) => {
        let envio_item = snapshot.val();
        if(!envio_item.historial) envio_item.historial = [];
        movimiento.fecha = new Date().toISOString();
        envio_item.historial.push(movimiento);
        snapshot.ref.child('historial').set(envio_item.historial);
        return res.send(envio_item);
    });
});

app.post('/:id/entregado', (req, res) => {
    let envio_item = admin.database().ref('/envios').child(req.params.id)
    envio_item.ref.child('pendiente').remove();
    return envio_item.once('value', (snapshot) => {
        return res.send(snapshot.val());
    });
});

// Funciones adicionales
//

// Expose Express API as a single Cloud Function:
exports.envios = functions.https.onRequest(app);
